package com.demo

class Country {

	String countryId
	String name
	String isoCode
	Date dateCreated
	Date lastUpdated
	String status = true
    static constraints = {
    }
}
