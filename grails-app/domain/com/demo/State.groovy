package com.demo

class State {

	String name
	String abbreviation
	String countryId
	Date dateCreated
	Date lastUpdated
	String status = true
    static constraints = {
    }
}
