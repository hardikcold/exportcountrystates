import org.codehaus.groovy.grails.web.context.ServletContextHolder

import com.demo.Country
import com.demo.State

class BootStrap {

    def init = { servletContext ->
		
		saveCountriesList()
		saveStatesList()
		
    }
    def destroy = {
    }
	
	def saveCountriesList(){
		println "Going to save countries..."
		String fileLocation = ServletContextHolder.getServletContext().getRealPath('xmlFiles')
		String fileName = fileLocation + File.separator + "country.xml"
		
		def country = new XmlSlurper().parse(fileName)
		country.country.each() {
		  //new Country(countryId : it.country_id, name : it.name, isoCode : it.iso_code).save(flush : true)
			try {
				//new Country(countryId : "123", name : "India", isoCode : 'IN').save(flush : true)
				new Country(countryId : it.country_id.toString() , name : it.name.toString(), isoCode : it.iso_code.toString()).save(flush : true)
			} catch (Exception e) {
				e.printStackTrace()
			}
		}
		println "Country saved..."
	}
	
	def saveStatesList(){
		println "Going to save states..."
		String fileLocation = ServletContextHolder.getServletContext().getRealPath('xmlFiles')
		String fileName = fileLocation + File.separator + "state.xml"
		
		def state = new XmlSlurper().parse(fileName)
		state.state.each() {
		  //new Country(countryId : it.country_id, name : it.name, isoCode : it.iso_code).save(flush : true)
			try {
				//new Country(countryId : "123", name : "India", isoCode : 'IN').save(flush : true)
				new State(name : it.name.toString() , abbreviation : it.abbreviation.toString(), countryId : it.country_id.toString()).save(flush : true)
			} catch (Exception e) {
				e.printStackTrace()
			}
		}
		println "State saved..."
	}
}
